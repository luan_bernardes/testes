import * as React from "react";
import "./button.scss";
import { Link } from 'react-router-dom';

export interface ButtonProps {
    text?:string;
    color?:string
}

export const ButtonComponent = (props: ButtonProps ) => {
    const buttonStyle = {
        color: props.color
    };
    return (
        <div className="button-component + {props.class}" style={buttonStyle}>
            qweqweqwe
            <br/>
            <Link to="/sobre">Ir para a página sobre \o/</Link>
            { props.text }
            { props.color}
        </div>
    )
};