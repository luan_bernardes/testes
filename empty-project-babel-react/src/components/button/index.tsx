import * as React from "react";
import * as ReactDOM from "react-dom";

import { ButtonComponent } from "./button";

export function InitButtonComponent() {

    const buttonComponent = document.getElementById("ButtonComponent");
    buttonComponent && ReactDOM.render(
        <ButtonComponent state="error" color="#dqwdq" text="hello baby"/>,
        buttonComponent
    );

}