# empty-project-babel-png-react-svg-sass-typescript

Empty project.

## Building and running on localhost

First install dependencies:

```sh
npm install
```

To create a production build:

```sh
npm run build-prod
```

To create a development build:

```sh
npm run build-dev
```

## Running

Open the file `dist/index.html` in your browser

## Credits

Created with [createapp.dev - an online tool for creating webpack and parcel projects](https://createapp.dev/)
