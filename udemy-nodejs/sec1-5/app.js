const path = require('path')
const express = require('express')
const bodyParser = require('body-parser')

const app = express()

const adminRoutes = require('./routes/admin')
const shopRoutes = require('./routes/shop')

app.use(bodyParser.urlencoded({ extended: false }))

// pulic folder browser to access
app.use(express.static(path.join(__dirname, 'public')));

// 'admin' prefixo
app.use('/admin', adminRoutes)
app.use(shopRoutes)

// se nao entrar em nenhuma rota, erro!
app.use((req, res) => {
    res.status(404).sendFile(path.join( __dirname, 'views', '404.html'))
})

app.listen(3000)