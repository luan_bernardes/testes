const path = require('path')
const rootDir = require('../utils/path')
const express = require('express')

const router = express.Router();

router.get('/', (req, resp, next) => {
    console.log('first midleware1')
    resp.sendFile(path.join( rootDir, 'views', 'shop.html' ))
})

module.exports = router