import React, {Component} from 'react';
import ReactDOM from "react-dom";
import './App.css';

const isSearched = searchTerm => item =>
    item.circuitName.toLowerCase().includes(searchTerm.toLowerCase());


class App extends Component {

    constructor(props) {

        super(props);

        this.state = {
            list: [],
            searchTerm: '',
        };

        this.onSearchChange = this.onSearchChange.bind(this);
    }

    onSearchChange(event) {
        this.setState({
            searchTerm: event.target.value
        });
    }

    onDismiss(id) {

        const isNotId = item => item.objectID !== id;

        const updateList = this.state.list.filter(isNotId);

        this.setState({
            list: updateList
        })
    };

    componentDidMount(){
        fetch('http://ergast.com/api/f1/2017/circuits.json')
            .then((response)=> response.json())
            .then((response)=> {
                //console.log(response.MRData.CircuitTable.Circuits);
                this.setState({ list: response.MRData.CircuitTable.Circuits })
            })
    }

    render() {
        const { list, searchTerm } = this.state;

        return (

            <div>

                <form>
                    <input type="text"
                           placeholder="buscar"
                           onChange={this.onSearchChange}
                    />
                </form>



                {
                    list.filter(isSearched( searchTerm )).map(item =>
                        <div key={item.circuitId}>
                            <div>{item.circuitName}</div>
                            <button onClick={ () => { this.onDismiss(item.objectID) } }
                                    type="button"
                            >
                                Dismiss
                            </button>
                            <br/>
                        </div>
                    )
                }
            </div>

        );
    }
}

export default App;
