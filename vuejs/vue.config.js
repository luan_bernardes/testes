const path = require("path");

module.exports = {
    css: {
        extract: true,
        loaderOptions: {
            sass: {
                data: `@import "@/styles/_variables.scss";`
            }
        },
        sourceMap: process.env.NODE_ENV === "development",
    },
    outputDir: process.env.NODE_ENV === "development"//por padrao vue-cli gera pastas diferentes
        ? path.resolve(__dirname, "../static/js")
        : '../static/',
    assetsDir: process.env.NODE_ENV === 'development'
        ? "../"
        : '',
    filenameHashing: false
};
